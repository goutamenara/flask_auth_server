import common.file_search as file_search
from importlib.machinery import SourceFileLoader
import os
import app_logger

RESOURCES_PATH = "modules" # Relative to project folder

log = app_logger.get_logger(__name__)


# function to create all api resource points in modules
# this requires a create_resource function defined in modules with resource_url definitions
def create_all(api):
    folder_path = os.getcwd()+"/"+RESOURCES_PATH
    pattern = "[!__]*[!__].py"
    files = file_search.files(folder_path,pattern)
    for file_path in files:
        filename = os.path.basename(file_path)
        resource = SourceFileLoader(filename , file_path).load_module()
        try:
            resource.create_resource(api)
        except Exception as e:
            log.error(e)
    