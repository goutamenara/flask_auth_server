from mongoengine import connect,disconnect

def connect_db(database="AUTH"):
    connect(database)

def disconnect_db():
    disconnect()