from flask import make_response
import json

class api_response:

    def __create_response(self,data={},code=None,headers={}):
        if(data):
            response = make_response(data)
        else:
            response = make_response()
            
        if(code):
            response.status_code = code

        if(headers):
            for key in headers.keys():
                response.headers.set(key,headers[key])

        return response

    def ok(self,**kwargs):
        return self.__create_response(code=200,**kwargs)

    def created(self,**kwargs):
        response = self.__create_response(code=201,**kwargs)
        return response

    def accepted(self,**kwargs):
        return self.__create_response(code=202,**kwargs)
    
    def no_content(self,**kwargs):
        return self.__create_response(code=204,**kwargs)


    # 3XX Responses -----------------------------------------------

    # 4XX Responses -----------------------------------------------

    def bad_request(self,**kwargs): 
        return self.__create_response(code=400,**kwargs)

    def unauthorized(self,**kwargs): 
        return self.__create_response(code=401,**kwargs)

    def forbidden(self,**kwargs):
        return self.__create_response(code=403,**kwargs)

    def not_found(self,**kwargs):
        return self.__create_response(code=404,**kwargs)

    def method_not_allowed(self,**kwargs):
        return self.__create_response(code=405,**kwargs)
    
    def not_acceptable(self,**kwargs):
        return self.__create_response(code=406,**kwargs)

    def conflict(self,**kwargs):
        return self.__create_response(code=409,**kwargs)

    def unsupported_media_type(self,**kwargs):
        return self.__create_response(code=415,**kwargs)

    # 5XX Response ------------------------------------------------------------

    def internal_server_error(self,**kwargs):
        return self.__create_response(code=500,**kwargs)