from mongoengine import *
from datetime import datetime

class user(Document):
    meta = {'collection' : 'users'}

    username = StringField(required=True,unique=True)
    password = StringField()
    token = StringField()
    
    created_at = DateTimeField()
    updated_at = DateTimeField(default=datetime.now())