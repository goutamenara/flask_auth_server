#!.env/bin/python3

# main imports
from flask import Flask
from flask_restful import Api
import json
import common.resource_creator as resource_creator

# logger initialize
import app_logger
log = app_logger.get_logger(__name__)

log.debug("Initializing Server")
# Python Server initialization
app = Flask(__name__)
# Initialize Flask Restful API
api = Api(app,catch_all_404s=True)

# add resources
log.debug("creating resources")
resource_creator.create_all(api)

# Check config
log.debug("loading configuration")
with open('config.json') as f:
    config = json.load(f)


if(__name__ == '__main__'):
    if(config["server"]):
        log.info("Server Started")
        app.run(**config["server"])
    else:
        log.info("Server Started")
        app.run()
        
