from flask_restful import Resource
import app_logger

log = app_logger.get_logger(__name__)

# Resource Methods
class Login(Resource):
    def post(self):

        from schema.user import user
        from common.api_response import api_response
        from flask import request
        import json

        args = request.args.to_dict()

        if 'username' in args.keys():
            
            from common.db_connect import connect_db
            connect_db()
            
            user_doc = user.objects(username=args["username"]).first()
            if(user_doc):
                if 'password' in args.keys():
                    if not(user_doc.password == args["password"]):
                        response_doc = {}
                        response_doc["message"] =  "Invalid Password"
                        return api_response().ok(data=json.dumps(response_doc))
                    response_doc = {}
                    response_doc["data"] = json.loads(user_doc.to_json())
                    import jwt
                    with open('config.json') as f:
                        config = json.load(f)
                    token = jwt.encode({"user" : user_doc.username},config["jwt"]["key"],algorithm='HS256').decode('UTF-8')
                    response_doc["data"]["token"] = token
                    user_doc["token"] = token
                    user_doc.save()
                    return api_response().ok(data=json.dumps(response_doc))
                else:
                    response_doc = {}
                    response_doc["message"] =  "Password Required"
                    return api_response().ok(data=json.dumps(response_doc))
            else:
                response_doc = {}
                response_doc["message"] =  "User not found"
                return api_response().ok(data=json.dumps(response_doc))

# function to add resource
# requirement for auto importing resources from modules
def create_resource(api):
    api.add_resource(Login,'/login',methods=['POST'])