from flask_restful import Resource
import app_logger

log = app_logger.get_logger(__name__)


class Logout(Resource):
    def get(self):
        from common.api_response import api_response
        from flask import request
        
        if("Authorization" in request.headers.keys()):
            import jwt
            import json
            with open('config.json') as f:
                config = json.load(f)
            try:
                decoded = jwt.decode(request.headers["Authorization"].encode('utf-8'), config["jwt"]["key"], algorithms='HS256')
                log.debug("token decoded : {}".format(decoded))
                if("user" in decoded.keys()):
                    from schema.user import user
                    from common.db_connect import connect_db
                    connect_db()
                    user_doc = user.objects(username=decoded["user"]).first()
                    if(user_doc):
                        if(user_doc.token):
                            if(user_doc.token == request.headers["Authorization"]):
                                user_doc.token = ""
                                user_doc.save()
                                response_doc = {}
                                response_doc["message"] = user_doc.username + " logged out successfully"
                                return api_response().ok(data=json.dumps(response_doc))
                return api_response().unauthorized()
                
            except Exception as e:
                log.error(e)
                return api_response().unauthorized()
        
        return api_response().unauthorized()




def create_resource(api):
    api.add_resource(Logout,'/logout',methods=['GET'])