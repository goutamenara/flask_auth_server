from flask_restful import Resource
import app_logger

log = app_logger.get_logger(__name__)

class SignUp(Resource):

    def post(self):
        from flask import request
        from common.api_response import api_response
        args = request.args.to_dict()
        
        if 'username' in args.keys():
            # check username already exists or not?
            from schema.user import user
            from common.db_connect import connect_db
            
            connect_db()
            temp_user = user.objects(username=args["username"])
            if(temp_user):
                return api_response().conflict()
            else:
                user_doc = user(username=args["username"])
                if'password' in args.keys():
                    user_doc.password = args["password"]
                else:
                    user_doc.password = args["username"]
                
                from datetime import datetime
                user_doc.created_at = datetime.now()
                user_doc.save()
                response_doc = {}
                import json
                response_doc["data"] = json.loads(user_doc.to_json())
                return api_response().ok(data=json.dumps(response_doc))


def create_resource(api):
    api.add_resource(SignUp,'/signup',methods=['POST'])